var _ = require('lodash-node');

exports.init = function(req, res) {
   res.setHeader('content-type', 'text/xml');

   if(!_.isUndefined(req.headers['origin'])) {
       res.setHeader('Access-Control-Allow-Credentials', true);
       res.setHeader('Access-Control-Allow-Origin', req.headers['origin']);
       res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
   } else {
       res.setHeader('Access-Control-Allow-Origin', '*');
       res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
   }

   res.write('<?xml version="1.0"?><cross-domain-policy><allow-access-from domain="*" /></cross-domain-policy>');
        res.flush();
        res.end();
};
