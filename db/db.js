var schema = require('./schemas');

exports.getAdvertisers = function(dealType, callback) {
	var Advertisers = schema.advertisers;

	myCache.get("advertisers", function(err, value) {
		if (!err) {
			if ( value == undefined) {

				Advertisers.aggregate([{$unwind: "$deals"},
										{$match: {$and: [{"status": "active"}, {"deals.status": "active"}, {"deals.dealType": dealType}] }},
										{$project: {"name": 1, "deals._id": 1, "deals.name": 1, "deals.dealType": 1, "deals.endPoint": 1, "deals.payout": 1}}], function(err, obj) {

										if (!err) {
			                	 			myCache.set("advertisers", obj, function( err, success) {
			                	 				if ( !err && success ) {
				                	 				callback(obj);
				                	 			}
				                	 		});

								 		} else {
								 			callback();
								 		}
				});

			} else {
				callback(value);
			}
		}
	});
}

exports.getSupplySource = function(req, supplyType, callback) {
	var Publishers = schema.publishers;

	myCache.get(req.params.tagId, function(err, value) {
		if (!err) {
			if ( value == undefined) {

				Publishers.aggregate([{$unwind: "$bids"}, {$match: { $and: [{_id: mongoose.Types.ObjectId(req.query.pid)},
								     { "bids._id": mongoose.Types.ObjectId(req.params.tagId) }] }},
									 {$match: {$and: [{"bids.status": "active"}, {"bids.supplyType": supplyType }] }},
									 {$project: {"pubName": 1, "bids._id": 1, "bids.supplyName": 1, "bids.environment": 1, "bids.target.endpoint": 1, "bids.minBid": 1}}], function(err, obj) {

				                	 if (!err) {
				                	 	myCache.set(req.params.tagId, obj[0], 3600, function( err, success) {
				                	 		if ( !err && success ) {
				                	 			callback(obj[0]);
				                	 		}
				                	 	});

						             } else {
				                	 	callback();
						             }
				});

			} else {
				callback(value);
			}
		}
	});
}

exports.getCountryCode = function(countryAlfa2, callback) {
	var Countries = schema.countries;
	myCache.get(countryAlfa2, function(err, value){

		if (!err) {
			if (value == undefined) {

				Countries.find({"alpha-2": countryAlfa2}, "alpha-3", function(err, obj){
                    if (err || !obj || Object.keys(obj).length === 0) {
                    	//throw err;
                    	callback();

                	} else if (obj) {
		                myCache.set(countryAlfa2, obj[0]["alpha-3"], 3600, function( err, success ) {

		                        if( !err && success ) {
                                	callback(obj[0]["alpha-3"]);
	                            }
	                    });
                	}
                });

			} else{
				callback(value);
			}
		}
	});
}

exports.getCampaignsByDemandDeal = function(dealType, callback) {
	var Advertisers = schema.advertisers;

	myCache.get(dealType, function(err, value) {
		if (!err) {
			if ( value == undefined) {

				Advertisers.aggregate([{$unwind: "$deals"}, {$match: {"deals.dealType": dealType, "deals.status": "active"} }, 
					 				  {$unwind: "$deals.campaigns"}, {$unwind: "$campaigns"}, {$match: {"campaigns.status": "active"}}, 
									  {$project: { areEqual: {$eq: ["$campaigns._id", "$deals.campaigns"]}, "deals": 1, "campaigns": 1, "name": 1}},  
									  {$match: { "areEqual": true}}], function(err, obj) {

				                	 if (!err) {
				                	 	myCache.set(dealType, obj, 3600, function( err, success) {
				                	 		if ( !err && success ) {
				                	 			callback(obj);
				                	 		}
				                	 	});

						             } else {
				                	 	callback();
						             }
				});

			} else {
				callback(value);
			}
		}
	});
}
