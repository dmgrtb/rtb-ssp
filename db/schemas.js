var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectIdSchema = Schema.ObjectId;
var ObjectId = mongoose.Types.ObjectId;


var dspListSchema = new Schema({
	 "_id": Schema.ObjectId,
         "name": String,
	 "url": String
}, {collection: "DSPList"});

var PublisherSchema = new Schema({
              _id:   { type: ObjectIdSchema },
           status:   { type: String },
            pubId:   { type: String },
          pubName:   { type: String },
           seatId:   { type: String },
         endpoint:   { type: String },
  trafficSourceId:   { type: Number },
           macros:   { type: Schema.Types.Mixed },
             bids:   [ {_id: ObjectIdSchema, status: String, supplyType: String, minBid: Number, endpoint: String, supplyName: String, target: Schema.Types.Mixed} ]
}, { versionKey: false });

var DealsSchema = new Schema({
                 _id: { type: ObjectIdSchema },
              status: { type: String },
                name: { type: String },
            dealType: { type: String },
        pricingModel: { type: String },
            endPoint: { type: String },
              payout: { type: Number },
           campaigns: [{ type: ObjectIdSchema }],
});

var AdvertisersSchema = new Schema({
              _id:   { type: ObjectIdSchema, auto: true},
             name:   { type: String },
           status:   { type: String },
            deals:   [ DealsSchema ],
        // campaigns:   [ CampaignsSchema ],
        // creatives:   [ CreativesSchema ]
}, { versionKey: false });


var CountrySchema = new Schema({
                 _id: { type: ObjectIdSchema },
           "alpha-2": { type: String },
           "alpha-3": { type: String }
});

module.exports.publishers = mongoose.model("Publishers", PublisherSchema, "SSPList");
module.exports.dsps = mongoose.model("Dsps", dspListSchema, "DSPList");
module.exports.advertisers = mongoose.model("Adv", AdvertisersSchema, "advertisers");
module.exports.countries = mongoose.model("Country", CountrySchema, "countries");


