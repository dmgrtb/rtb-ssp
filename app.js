var index = require('./index');
var express = require('express');
var myParser = require("body-parser");
var app = express();
app.use(myParser.json());

var request = require('./requestStructure/buildRequestObject');
var vastRequest = require('./vastRequest/buildRequestObject');

var cluster = require('cluster');
http = require('http');
var os = require('os');
var numCPUs = os.cpus().length;
fs = require('fs');
//jsonfile = require('jsonfile');
file = "/home/rtb/logs/dsp.json";
var zlib = require("zlib");
isArray = require('util').isArray;
extend = require('util')._extend;

/* cache */
var NodeCache = require("node-cache");
myCache = new NodeCache({stdTTL: 100});

// gzip compression
var compress = require('compression');
app.use(compress({level: 6}));

sspLog = fs.createWriteStream(config.logstashPath, {'flags': 'a'});


/**
 * Get local server ip for logging purposes
 */
var ifaces = os.networkInterfaces();
var localServerIp = '';
Object.keys(ifaces).forEach(function (ifname) {

  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }
    if(localServerIp == ''){
      localServerIp = iface.address;
      console.log('localServerIp = ' + localServerIp);
    }
  });
});

sspLog = fs.createWriteStream(config.logstashPath, {'flags': 'a'});


/**
 * Mark the start timestamp of each request
 */
app.all('*', function(req, res, next) {
  req.startTime = Date.now();
  req.localServerIp = localServerIp;
  return next();
});


/**
 * Routers
 */
app.get('/st/:tagId', request.getTagDetails);

app.get('/vt/:tagId', vastRequest.getVastTagDetails);

app.get('/vimp/', function(req, res) {
  console.log(req);
});

//cross domain.
var crossDomain = require("./crossDomain.js");
app.get('/crossdomain.xml', crossDomain.init);


var server = app.listen(config.port, function () {
        var host = server.address().address;
        var port = server.address().port;
        console.log('http://%s:%s', host, port);
        console.log(process.env.NODE_ENV + ' MODE!');
});


