var common = require('./common');
config = common.config();
mongoose = require('mongoose');
mongoose.connect(config.mongoDb);
db = require('./db/db');
// sysLog = require('simple-node-logger').createSimpleLogger({logFilePath: config.sysLogPath ,timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'});
var winston = require('winston');
sysLog = new (winston.Logger)({
    transports: [
      new (winston.transports.File)({'filename': config.sysLogPath, 'timestamp':true})
    ]
});


