var async = require('async');
var mongoose = require('mongoose');
var propertiesArr = ['w', 'h', 'pos', 'api', 'battr', 'pos', 'mimes'];

exports.createVideo = function(req, tag, callback) {

	var video = {};
	video.pos = 0;

	async.each(propertiesArr, function(prop, callback) {


		if (prop == 'battr') {
			video.battr = [];
			callback();

		// } else if (prop == 'api') {
		// 	if (tag[prop]) {
		// 		video.api = [tag[prop]];
		// 	}
		// 	callback();

		} else if ( (prop == 'w' || prop == 'h') && req.query[prop] ) {
			video[prop] = parseFloat(req.query[prop]);
			callback();

		} else if (prop == 'mimes') {
		    video[prop] = ["video/x-flv",
					       "video/mp4",
						   "application/x-shockwave-flash",
						   "application/javascript"];
		    callback();

		} else if (req.query[prop]) {
			video[prop] = req.query[prop];
			callback();

		} else {
			callback();
		}

	}, function(err) {
	    // if any of the file processing produced an error, err would equal that error
	    if( err ) {
	      sysLog.error('video create error: ' + err);
	      callback();
	    } else {
	      callback(video);
	    }
	});
}