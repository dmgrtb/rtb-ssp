var async = require('async');
var mongoose = require('mongoose');
var propertiesArr = ['name', 'storeurl', 'bundle', 'domain'];

exports.createApp = function(req, callback) {

	var app = {};

	async.each(propertiesArr, function(prop, callback) {

		if (req.query[prop]) {
			app[prop] = req.query[prop];
			callback();

		} else {
			callback();
		}

	}, function(err) {
	    // if any of the file processing produced an error, err would equal that error
	    if( err ) {
	      sysLog.error('app create error: ' + err);
	      callback();
	    } else {
	      callback(app);
	    }
	});
}