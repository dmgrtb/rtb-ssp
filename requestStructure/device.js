var async = require('async');
var maxMind = require('./../services/maxmind');
var qouva = require('./../services/qouva');
var uap = require('node-uap');
var propertiesArr = ['geo', 'ua', 'carrier', 'ip', 'connectiontype', 'dnt'];

exports.createDevice = function(req, callback) {

	var device = {};
	device.dnt = 0;
	device.geo = {};
	var userAgentString;
	var osv = {};
	var deviceType = {};
	var connectionType = {};
	// maxMind.getByIp(req.query.ip, function(result) {
	// 	console.log(result);
	// });

	qouva.qouvaGetByIp(req.query.ip, function(result) {

		if (result && result.Location.CountryData.country_code) {
			var country = result.Location.CountryData.country_code.toUpperCase();
		}

		async.each(propertiesArr, function(prop, callback) {

			if (prop == 'geo' && country) {
				db.getCountryCode(country, function(alpha3) {
					device[prop].country = alpha3;
					callback();
				});

			} else if (prop == 'carrier' && result && result.Network.carrier) {
				if (result) {
					device[prop] = result.Network.carrier;
				}
				callback();

			} else if (prop == 'connectiontype' && result && result.Network.connection_type) {
				if (result && result.Network.connection_type == "dsl"){
					device[prop] = 1;
				} else if (result && result.Network.connection_type == "mobile wireless") {
					device[prop] = 5;
				} else {
					device[prop] = 0;
				}
				callback();

			} else if (prop == 'ua' && req.query[prop]) {
				//parse user agent string.
				userAgentString = uap.parse(req.query[prop]);
				device.ua = userAgentString.userAgent;
				device.make = userAgentString.device.brand;
				device.model = userAgentString.device.model;
				device.os = userAgentString.os.family;

				osv = userAgentString.os.major;
				if (userAgentString.os.minor){
					osv = osv + "." + userAgentString.os.minor;
				}

				if (userAgentString.os.patch){
					osv = osv + "." + userAgentString.os.patch;
				}

				device.osv = osv;

				if (userAgentString.os.family == "Android" || userAgentString.os.family == "iOS") {
					device.deviceType = 1;
				} else {
					device.deviceType = 2;
				}
				callback();

			} else if (req.query[prop]) {
				device[prop] = req.query[prop];
				callback();

			} else {
				callback();
			}

		}, function(err) {

		    // if any of the file processing produced an error, err would equal that error
		    if( err ) {
		      sysLog.error('device create error: ' + err);
		      callback();

		    } else {
		      callback(device);
		    }
		});
	});
}