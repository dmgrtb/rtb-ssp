var async = require('async');
var mongoose = require('mongoose');
var propertiesArr = ['id', 'bidfloor', 'instl', 'tagid', 'device'];

exports.createImp = function(req, tag, callback) {

	var imp = {};
	imp.tagid = tag.bids._id;
	imp.bidfloor = tag.bids.minBid;

	async.each(propertiesArr, function(prop, callback) {

		if (prop == 'id') {
			imp.id = mongoose.Types.ObjectId();
			callback();

		} else if (req.query[prop]) {
			imp[prop] = req.query[prop];
			callback();

		} else {
			callback();
		}

	}, function(err) {
	    // if any of the file processing produced an error, err would equal that error
	    if( err ) {
	      sysLog.error('imp create error: ' + err);
	      callback();
	    } else {
	      callback(imp);
	    }
	});
}


