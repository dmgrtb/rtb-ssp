var async = require('async');
var mongoose = require('mongoose');
var propertiesArr = ['id', 'imp', 'at', 'bcat', 'badv', 'app', 'site', 'device', 'at'];

exports.createBidRequest = function(req, callback) {

	var bidRequest = {};

	async.each(propertiesArr, function(prop, callback) {

		if (prop == 'id') {
			bidRequest.id = mongoose.Types.ObjectId();
			callback();

		} else if (prop == 'imp') {
			bidRequest.imp = [];
			callback();

		} else if (prop == 'at') {
			bidRequest.at = 2;
			callback();

		} else if (req.query[prop]) {
			bidRequest[prop] = req.query[prop];
			callback();

		} else {
			callback();
		}

	}, function(err) {
	    // if any of the file processing produced an error, err would equal that error
	    if( err ) {
	      callback();
	      sysLog.error('bid request create error');
	    } else {
	      callback(bidRequest);
	    }
	});
}
