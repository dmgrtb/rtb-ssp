var async = require('async');
var url = require('url');
var propertiesArr = ['id', 'domain', 'cat'];

exports.createSite = function(req, callback) {

	// console.log(req);

	var site = {};

	async.each(propertiesArr, function(prop, callback) {

		if (prop == 'cat' && req.query[prop]) {
			site[prop] = req.query[prop];
			callback();

		} else if (prop == 'id' && req.query.siteid) {
			site[prop] = req.query.siteid;
			callback();

		} else if (prop == 'domain' && req.query[prop]) {
			console.log(req.query[prop]);
			site[prop] = req.query[prop];
			callback();

		} else if (req.query[prop]) {
			site[prop] = req.query[prop];
			callback();

		} else {
			callback();
		}

	}, function(err) {
	    // if any of the file processing produced an error, err would equal that error
	    if( err ) {
	      sysLog.error('site create error: ' + err);
	      callback();

	    } else {
	      callback(site);
	    }
	});
}
