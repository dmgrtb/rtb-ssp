var async = require('async');
var bidRequest = require('./bidRequest');
var imp = require('./imp');
var video = require('./video');
var site = require('./site');
var device = require('./device');
var app = require('./app');
var auction = require('../auction/auctionManager');
var helper = require('../modules/sendRes');
var mongoose = require('mongoose');
var logger = require('../modules/logger.js');
var dsp = require('../auction/buildDspsRequestList');
var bid = require('../auction/rtbRequest');


exports.getTagDetails = function(req, res) {

    async.parallel({

        supplySource: function(callback) {
            db.getSupplySource(req, 'video-vast', function(tag) {
                if(tag) {     
                    // sysLog.info('====================TAG ' +  tag.bids.supplyName + ' FOUND====================');
                    callback(null, tag);
                } else {
                    callback(true, tag);
                    sysLog.error('====================TAG ' +  req.params.tagId + ' NOT FOUND===================');
                }
            });
        },

        dsps: function(callback) {
            dsp.buildDspsRequsetList('video-rtb', req, function(result) {
                if(result.length > 0) {
                    callback(null, result);
                } else {
                    callback(true, result);
                }
            });
        },
    },

    function(err, results) {
        if (!err ) {
            var loggerObj = logger.createLogger();
            loggerObj = logger.setPublisher(loggerObj, results.supplySource);
            loggerObj = logger.setSupplySource(loggerObj, results.supplySource);
            buildRequestObject(req, res, results.supplySource, results.dsps, loggerObj);
        } else {
            helper.sendHttpStatus(res, 204);
        }
        
    });  
}


function buildRequestObject(req, res, tag, dsps, loggerObj) {

    async.parallel({

        bidRequest: function(callback) {
            bidRequest.createBidRequest(req, function(result) {
                callback(null, result);
            });
        },

        imp: function(callback) {
            imp.createImp(req, tag, function(result) {
                callback(null, result);
            });
        },

        video: function(callback) {
            video.createVideo(req, tag, function(result) {
                callback(null, result);
            });
        },

        site: function(callback) {
            site.createSite(req, function(result) {
                callback(null, result);
            });
        },

        app: function(callback) {
            app.createApp(req, function(result) {
                callback(null, result);
            });
        },

        device: function(callback) {
            device.createDevice(req, function(result) {
                callback(null, result);
            });
        },

        // function(callback) {
        //     createGeo(bidRequest, function(result) {
        //         callback('geo', result);
        //     });
        // }

        // forensiqScore: function(callback) {
        //     forensiq.check(req, loggerObj, tag, function(result) {
        //         callback(null, result);
        //     });
        // }
    },

    function(err, results) {
        if ( !err ) {
            buildRtbRequest(res, req, results, dsps, tag, loggerObj); 
        } else {
            helper.sendHttpStatus(res, 204);
        }
        
    });
}


function buildRtbRequest(res, req, results, dsps, tag, loggerObj) {
    var rtbRequest = {};
        rtbRequest = results.bidRequest;
        rtbRequest.imp.push(results.imp);
        rtbRequest.imp[0] = Object.assign(rtbRequest.imp[0],{video: results.video});
        rtbRequest.device = results.device;
        if (tag.bids.environment == 'web') {
            rtbRequest.site = results.site;
        } else if (tag.bids.environment == 'inapp') {
            rtbRequest.app = results.app;
        }
        loggerObj = logger.setAucReq(loggerObj, rtbRequest);  
        bid.initBidRequest(dsps, rtbRequest, res, req, loggerObj, auction.chooseWinner);
        //auction.run(res, req, rtbRequest, loggerObj);
}

