exports.loggerParse = function(loggerObj) {

	var loggerObj = loggerObj;
	var isWinner = false;
	var tempLog = {};
	var originalLogObj = {};

	originalLogObj = JSON.parse(JSON.stringify(loggerObj));

	//copy original log to temp log.
	tempLog = JSON.parse(JSON.stringify(loggerObj));

	//delete win field if exist to exclude the winner document.
	if (tempLog.auction.hasOwnProperty('winDspId')) {
		isWinner = true;
		delete tempLog.auction.winDspId;
		delete tempLog.auction.winBid;
		delete tempLog.auction.win2Bid;
	}

	for (var i=0, length = originalLogObj.auction.dsp.length; i < length; i++) {

		tempLog.auction.dsp = originalLogObj.auction.dsp[i];

		if (isWinner && originalLogObj.auction.dsp[i]._id === originalLogObj.auction.winDspId) {
			var tempWinnerLog = JSON.parse(JSON.stringify(tempLog));
			tempWinnerLog.auction.winDspId = originalLogObj.auction.winDspId;
			tempWinnerLog.auction.winBid = originalLogObj.auction.winBid;
			tempWinnerLog.auction.win2Bid = originalLogObj.auction.win2Bid;

			isWinner = false;
			writeLog(tempWinnerLog);
		} else {
			writeLog(tempLog);
		}
	}

	//if no responses was recevied.
	if (originalLogObj.auction.dsp.length === 0) {
		writeLog(tempLog);
	}

}

function writeLog(log) {
	if (log) {
		var sspLog = fs.createWriteStream(config.logstashPath, {'flags': 'a'});
	    sspLog.write(JSON.stringify(log) + "\n");
	    sspLog.end();
	    return;
	}
}