var request = require('request');
var http = require('http');

exports.get = function (host, path, headers=null, callback) {

    var options = {
        host: host,
        path: path,
        method: 'GET',
        headers: {headers}
    };

    var callbackFunc = function(response) {
      var str = ''

      if (response.statusCode != 200) {
          callback();
      } else {
          response.on('data', function (chunk) {
            str += chunk;
          });

          response.on('end', function () {
            callback(str);
          });
      } 
    }

    var req = http.request(options, callbackFunc);

    req.on('socket', function (socket) {
      socket.setTimeout(100);  
      socket.on('timeout', function() {
          req.abort();
      });
    });

    req.on('error', function (err) {
      if (err.code === "ECONNRESET") {
        console.log("Timeout occurs");
        callback('timeout exceeded');
      } else {
        console.log('problem with request: ' + err.message);
        callback('unknown error');
      }
    });

    req.end();
}