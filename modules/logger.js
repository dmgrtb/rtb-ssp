exports.createLogger = function() {
  var obj = {
	      publisher:{},
        supplySource:{},
	      auction:{
	          dsp: []
	      }
	  }
  return obj;
}

exports.setPublisher = function(obj, tag) {
  obj.publisher = {};
  obj.publisher = tag.pubName;
  return obj;
}

exports.setSupplySource = function(obj, tag) {
  obj.supplySource = {};
  obj.supplySource = tag.bids.supplyName;
  return obj;
}

function getCurrentTime() {
  var currdatetime = new Date();
  return currdatetime;
}

exports.setAucReq = function(obj, aucReq) {
  obj.auction = {};
  obj.auction.time = getCurrentTime();
  obj.auction.request = aucReq;
  return obj;
}

exports.setAucDspsRes = function(obj, dspRes) {
  if (!obj.auction.dsp) {
  	obj.auction.dsp = [];
  }
  obj.auction.dsp.push(dspRes);
  return obj;
}

exports.setWinDsp = function(obj, winDsp, dsp, secondBidAuc) {
  obj.auction.winDspId = dsp._id;
  obj.auction.winBid = winDsp.seatbid[0].bid[0].price;
  obj.auction.win2Bid = secondBidAuc;
  return obj;
}

exports.setWinNotice = function(obj, winNotice) {
  obj.auction.winNotice = winNotice;
  obj.auction.winNoticeTime = getCurrentTime();
  return obj;
}

exports.setWinNoticeError = function(obj, err) {
  obj.auction.winNoticeError = err;
  return obj;
}

exports.setDemandPartner = function(obj, adv) {
  obj.auction.demandPartnerId = adv._id
  obj.auction.demandPartner = adv.name;
  obj.auction.demandDeal = adv.deals.name;
  obj.auction.endPoint = adv.deals.endPoint;
  return obj;
}

