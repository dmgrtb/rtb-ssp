exports.macrosAnalyser = function macrosAnalyser(string) {
	string = string.replace(/\$/g, "");
	return string;
}


exports.analyse = function (req, string) {

    string = string.replace('${width}', req.query.w);
    string = string.replace('${height}', req.query.h);
    string = string.replace('${useragent}', req.query.ua);
    string = string.replace('${ip}', req.query.ip);
    string = string.replace('${dnt}', req.query.dnt);
    string = string.replace('${appname}', req.query.appname);
    string = string.replace('${bundleid}', req.query.bundleid);
    string = string.replace('${deviceid}', req.query.deviceid);
    string = string.replace('${pageurl}', encodeURIComponent(req.query.pageurl));
    string = string.replace('${appstoreurl}', req.query.appstoreurl);
    string = string.replace('${cachebuster}', req.query.cb);
    string = string.replace('${iploclat}', req.query.iploclat);
    string = string.replace('${iploclong}', req.query.iploclong);
    string = string.replace(/\${domain}/g, encodeURIComponent(req.query.domain));
    string = string.replace('${categoryiab}', req.query.categoryiab);
    string = string.replace('${refurl}', encodeURIComponent(req.headers.referer));
    string = string.replace('${cookieid}', req.query.cookieid);
    string = string.replace('${country}', req.query.geocountry);
    string = string.replace('${aid}', req.query.aid); 
    string = string.replace('${idfasha1hex}', req.query.idfasha1hex);
    string = string.replace('${demandpartner}', req.query.demandpartner);
    string = string.replace('${demanddeal}', req.query.demanddeal);
    string = string.replace('${publisher}', req.query.publisher);
    string = string.replace('${campaign}', req.query.campaign);

    return string;
}