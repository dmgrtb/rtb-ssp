exports.sendHttpStatus = function(res, status) {
    res.status(status).end();
}

exports.sendJson = function(res, status, data) {
    res.status(status).json(data);
}

exports.sendXml = function(req, res, status, data) {

	if (req.headers['origin']) {
		res.set('Access-Control-Allow-Credentials', 'true');
        res.set('Access-Control-Allow-Origin', req.headers['origin']);
    } else {
        res.set('Access-Control-Allow-Origin', '*');
    }

	res.set({
		'Cache-Control': 'no-cache',
		'Content-Type': 'text/xml; charset=utf-8',
		'Connection': 'keep-alive'
	});

    res.status(status).send(data);
    res.end();
}


