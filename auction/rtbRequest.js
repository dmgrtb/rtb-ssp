var request = require('request');
var async = require('async');
var validator = require('./rtbResponseValidator');
var logger = require('../modules/logger.js');
var forensiq = require('../services/forensiq');


exports.initBidRequest = function(dspArray, rtbRequest, response, req, loggerObj, cb) {
	var loggerNewObj = {};
	var tempArray = [];

	var fetch = function(dsp, callback) {

		var dspObj = {};
	        dspObj._id = dsp._id;
	        dspObj.demandPartner = dsp.name;
	        dspObj.demandDeal = dsp.deals.name;
	        dspObj.demandDealType = dsp.deals.dealType;
			dspObj.endPoint = dsp.deals.endPoint;
			dspObj.campaign = dsp.campaigns.name;

		//forensiq check.
		if (dsp.deals.forensiq == 'active') {
			forensiq.check(req, loggerObj, dspArray, function(result) {
				dspObj.forensiqScore = result;
				console.log('=======Forensiq ' + result + '=======');
				if(result < config.forensiqRiskScore && result >= 0) {
					bidRequest();
				} else {
					dspObj.responseStatus = 'abort request';
					loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
					callback(null, null);
				}
			});
		} else {
			bidRequest();
		}


		function bidRequest() {
			
			request({
			  method: 'POST',
			    time: true,
	    	     uri: dsp.deals.endPoint,
	    	 timeout: config.timeout,
	    	 headers: 'Connection: keep-alive',
			    json: rtbRequest },
			    	function(error, response, body) {

	            // && response.statusCode >= 200 && response.statusCode < 300

					if (!error && body) {
						validator(body, rtbRequest, function(result) {
							if (result) {
								dspObj.responseLatency = response.elapsedTime;
								dspObj.responseStatus = 'validation success';
								dspObj.response = body;
								tempArray.push(dspObj);
								loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
								callback(null, body);
							} else {
								dspObj.responseStatus = 'validation fail';
								dspObj.response = body;
								loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
								callback(null, body);
							}
						});

					} else if (!error && !body) {
						dspObj.responseStatus = 'no response';
						loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
						callback(null, body);

					} else if (error.code === 'ETIMEDOUT') {
						dspObj.responseStatus = 'timeout exceeded';
						loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
						callback(null, body);

					} else {
						dspObj.responseStatus = 'unknown error';
						dspObj.error = error;
						loggerNewObj = logger.setAucDspsRes(loggerObj, dspObj);
						callback(null, body);
					}
			    });
		}	
	}

	async.map(dspArray, fetch, function(err, results) {
		if (err) {
		  sysLog.error('error has occurred!');
		} else {
		  cb(tempArray, response, req, rtbRequest, loggerNewObj);
		}

	});
};





