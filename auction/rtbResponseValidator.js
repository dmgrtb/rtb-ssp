validator = function(response, jsnoReq, callback) {

	if ( response && response.id !== undefined && response.seatbid !== undefined && response.seatbid[0].bid !== undefined ) {

		validateBid(response, jsnoReq, function(result) {
			if (result) {
				callback(true);
			} else {
				callback(false);
			}
		});
	} else {
		callback(false);
	}
}

//IAB BidResponse object.
function validateBidResponse() {

}

//IAB SeatBid object.
function validateSeatBid() {

}

//IAB Bid object.
function validateBid(response, jsnoReq, callback) {
	if (response.seatbid[0].bid[0].hasOwnProperty('id') && response.seatbid[0].bid[0].hasOwnProperty('impid') && response.seatbid[0].bid[0].hasOwnProperty('price') &&
		response.seatbid[0].bid[0].price >= jsnoReq.imp[0].bidfloor) {
		callback(true);
	} else {
		callback(false);
	}
}

module.exports = validator;