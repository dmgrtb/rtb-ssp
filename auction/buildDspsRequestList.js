var async = require('async');

exports.buildDspsRequsetList = function(dealType, req, callback) {

  db.getCampaignsByDemandDeal(dealType, function(campaignsArr) { 

        var dspArray = [];
        var dspTempArray = [];

        async.each(campaignsArr, function(campaign, callback) {

            if (campaign.campaigns.target.supplySource) {
                
                asyncEach(campaign.campaigns.target.supplySource, req.params.tagId, function(result) {

                    if (result) {

                        if (!dspTempArray[campaign.deals._id]) {
                            dspArray.push(campaign);
                            dspTempArray[campaign.deals._id] = campaign.deals;
                        }
                    }
                    callback(); 
                });

            } else if (campaign.campaigns.target.supplySource_black) {
                    // console.log(campaign.campaigns.target.supplySource_black[0]);
                    // asyncEach(campaign.campaigns.target.supplySource_black, req.params.tagId, function(result) {

                    // supplySource_black = result;
                    callback();
                
            } else {
                    if (!dspTempArray[campaign.deals._id]) {
                      dspArray.push(campaign);
                      dspTempArray[campaign.deals._id] = campaign.deals;
                    }
                    callback();
            }

        }, function(err) {
            // if any of the file processing produced an error, err would equal that error
            if( err ) {
              callback();
              sysLog.error('dsps requset list error');
            } else {
              callback(dspArray);
            }
        });
    });
}


function asyncEach(supplySourceList, tagId, callback) {

    async.each(supplySourceList, function(supplySourceId, callback) {
        if (supplySourceId == tagId) {
            callback(true);

        } else {
            callback();
        } 

    }, function(err) {
        // if any of the file processing produced an error, err would equal that error
        if( err ) {
          callback(err);
        } else {
          callback();
        }
    });

}

