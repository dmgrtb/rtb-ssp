var parseString = require('xml2js').parseString;
var xml2js = require('xml2js');
var _ = require('lodash');
var request = require('request');
var bid = require('./rtbRequest');
var util = require('util');
var helper = require('../modules/sendRes');
var logger = require('../modules/logger.js');
var loggerParse = require('../modules/loggerParse');
var dsp = require('./buildDspsRequestList');


exports.chooseWinner = function(responses, response, req, rtbRequest, loggerObj) {
    var winningBid = 0;
    var secondBid = -1;
    winnerIndex = -1;
    for (var i=0; i < responses.length; i++) {
        if (responses[i].response.seatbid[0].bid[0].price &&
            winningBid < responses[i].response.seatbid[0].bid[0].price) {
            winningBid = responses[i].response.seatbid[0].bid[0].price;
            winnerIndex = i;

          //second hand bid.
        } else if (secondBid < responses[i].response.seatbid[0].bid[0].price) {
            secondBid = responses[i].response.seatbid[0].bid[0].price;
        }
    }

    //set secondBid as bidfloor when single response was made.
    if (responses.length === 1) {
        secondBid = loggerObj.auction.request.imp[0].bidfloor;
    }

    if (winnerIndex >= 0) {
        sysLog.info('A winning bid has been chosen: ' + util.inspect(responses[winnerIndex].response, {showHidden: false, depth: null}));
        loggerObj = logger.setWinDsp(loggerObj, responses[winnerIndex].response, responses[winnerIndex], secondBid);
        if (responses[winnerIndex].response.seatbid[0].bid[0].hasOwnProperty('adm')) {
            buildWinNotice(responses[winnerIndex].response, response, rtbRequest, winNoticeDeafult);
            var adm = responses[winnerIndex].response.seatbid[0].bid[0].adm;
            var vast = decodeURIComponent(adm);
            macrosAnalyser(vast, responses[winnerIndex].response, rtbRequest, function(newVast) {
                convertXmlString(newVast, response, req, loggerObj, rtbRequest, addImpressionTracker);
            });
        } else {
            sysLog.info('Building a Win Notice...');
            buildWinNotice(responses[winnerIndex].response, response, rtbRequest, function(nurl, res) {
                winNotice(loggerObj, rtbRequest, nurl, req, res);
            });
        }
    } else {
        sysLog.info('Fail to choose a winning bid.');
        helper.sendHttpStatus(response, 204);
        loggerParse.loggerParse(loggerObj);
    }
}

function buildWinNotice(winnerReponse, response, rtbRequest, callback) {
    winBidderResponse = winnerReponse;
    bidderNurl = winnerReponse.seatbid[0].bid[0].nurl;

    macrosAnalyser(bidderNurl, winnerReponse, rtbRequest, function(newNurl) {
        sysLog.info('Format win response: ' + newNurl);
        callback(newNurl, response);
    });
}

function macrosAnalyser(string, winnerReponse, rtbRequest, callback) {
    sysLog.info('Applying macro analyser...');
    string = string.replace('${AUCTION_ID}', rtbRequest.id);
    string = string.replace('${AUCTION_BID_ID}', winnerReponse.bidid);
    string = string.replace('${AUCTION_IMP_ID}', rtbRequest.imp[0].id);
    string = string.replace('${AUCTION_SEAT_ID}', winnerReponse.seatbid[0].seat);
    string = string.replace('${AUCTION_AD_ID}', winnerReponse.seatbid[0].bid[0].adid);
    string = string.replace('${AUCTION_PRICE}', winnerReponse.seatbid[0].bid[0].price);
    string = string.replace('${AUCTION_CURRENCY}', 'USD');

    callback(string);
}

//TODO: check if timeout is require in win notification.
function winNotice(loggerObj, rtbRequest, nurl, req, res) {
    sysLog.info("Sending Win Notice: " + nurl);
    request(
    { method: 'GET',
      uri: nurl
    },
    function(error, response, body) {
        if (!error && response.statusCode == 200) {
            // var adm = body;
            // var vast = decodeURIComponent(adm);
            macrosAnalyser(body, winBidderResponse, rtbRequest, function(newVast) {
                convertXmlString(newVast, res, req, loggerObj, rtbRequest, addImpressionTracker);
            });
            helper.sendXml(req, response, 200, body);

        } else {
            sysLog.error('Returned bad win notice response: ' + err);
        }
    });
}

function winNoticeDeafult(nurl, response) {
    sysLog.info("Sending Win Notice to: " + nurl);
    request.get(nurl).on('error', function(err) {
        sysLog.error('win notice error: ' + err);
    });
}

function convertXmlString(xmlString, response, req, loggerObj, rtbRequest, callback) {
    parseString(xmlString, function (err, result) {
        if (err){
            sysLog.error('failed to parse xml vast reposnse: ' + err);
            helper.sendHttpStatus(response, 204);
            loggerParse.loggerParse(loggerObj);
        } else {
            auctionid = rtbRequest.id;
            impressionUrl = config.eventTrackerUrl + '?auctionid=' + auctionid + '_' + loggerObj.auction.winDspId + '&id=0';
            sysLog.info('success parse xml vast response.');
            callback(result, impressionUrl, auctionid, response, req, loggerObj);
        }
    });
}


function addImpressionTracker(convertedJson, impressionUrl, auctionid, response, req, loggerObj) {

    if(convertedJson && convertedJson.VAST !== undefined && convertedJson.VAST.Ad instanceof Array) {

        _.forEach(convertedJson.VAST.Ad, function(ad) {
            /**
             * Get the reference to the ad, either wrapper or linear
             */
            var adBody;
            if (ad.InLine instanceof Array) {
                adBody = ad.InLine;
            } else if (ad.Wrapper instanceof Array) {   
                adBody = ad.Wrapper;
            }
            // Validate the ad array
            if(adBody instanceof Array) {

                // If the Impressions tag does not exists or it an empty tag replace it with array
                if(!(adBody[0].Impression instanceof Array) || (adBody[0].Impression.length == 1 && adBody[0].Impression[0].trim() == '')) {
                    adBody[0].Impression = impressionUrl;
                } else {
                    // Add our own impression url
                    adBody[0].Impression.unshift({
                        _: impressionUrl,
                        '$': {id: auctionid + '_' + loggerObj.auction.dsp._id}
                    });
                }

                // Add event trackers to each creative
                if(adBody[0].Creatives instanceof Array && adBody[0].Creatives[0].Creative instanceof Array) {
                    _.forEach(adBody[0].Creatives[0].Creative, function(creative){

                        _.forEach(creative, function(adType, key) {

                            if(adType instanceof Array){
                                if ( !(adType[0].TrackingEvents instanceof Array) ||typeof adType[0].TrackingEvents[0] !== 'object'){
                                    adType[0].TrackingEvents = [{}];
                                }
                                if ( !(adType[0].TrackingEvents[0].Tracking instanceof Array) ){
                                    adType[0].TrackingEvents[0].Tracking = [];
                                }
                        
                                adType[0].TrackingEvents[0].Tracking.unshift({
                                    _: impressionUrl,
                                    '$': {event: 'start'}
                                });
                            }
                        });
                    });
                }
            }
        });
        
        var builder = new xml2js.Builder({cdata: true, headless: true});
        xmlString = builder.buildObject(convertedJson);
        sysLog.info('successfully added impression tracker to xml vast reponse.');
        helper.sendXml(req, response, 200, xmlString);
        loggerParse.loggerParse(loggerObj);
    } else {
        sysLog.error('failed to add impression tracker to xml vast reponse.');
        helper.sendHttpStatus(response, 204);
    }
}

