var restRequest = require('../modules/restRequests');

function ready() {
	console.log(restRequest.get(config.forensiq + 'ready?' + config.forensiqClientKey, 'Connection: keep-alive'));
	return;
}

exports.check = function(req, loggerObj, adv, callback) {
	var ttl = 86400;

	if ( req.query.ip && req.query.rt && (req.query.rt == 'display' || req.query.rt == 'click') && (req.query.pageurl || req.query.aid) ) {

		if (req.query.pageurl) {
			var urlOrAid = 'url=' + encodeURIComponent(req.query.pageurl);
		} else if (req.query.aid) {
			var urlOrAid = 'aid=' + req.query.aid;
		}

		var queryString = '';
		queryString = '/check?ck=' + config.forensiqClientKey + '&output=json&';
		queryString += 'ip=' + req.query.ip + '&' + 'rt=' + req.query.rt + '&' + urlOrAid + '&' + 
		               'seller=' + loggerObj.publisher + '&' + 'sub=' + loggerObj.supplySource  + '&' + 'cmp=' + adv[0].deals.name;

		myCache.get(queryString, function(err, value) {

			if (!err) {
				if (value == undefined) {

					restRequest.get(config.forensiq ,queryString, 'Connection: keep-alive', function(score) {

						try {
							var score = JSON.parse(score);

							if (!isNaN(score.riskScore)) {

								if (score.riskScore < config.forensiqRiskScore && score.riskScore >= 0) {
									ttl = 600;
								}

								myCache.set(queryString, score, ttl, function( err, success ) {

				                        if( !err && success ) {
		                                	callback(score.riskScore);
			                            }
			                    });
							} else {
								callback(-1);
							}

						} catch (e) {
							callback(-1);
						}

				
					});

				} else {
					console.log('===========' + value.riskScore + '===========');
					callback(value.riskScore);
				}

			} else {
				callback();
			}
		});

	} else {
		callback();
	}
}


