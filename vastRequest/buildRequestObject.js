var mongoose = require('mongoose');
var logger = require('../modules/logger.js');
var vastRequest = require('./vastRequest');
var helper = require('../modules/sendRes');

exports.getVastTagDetails = function(req, res) {
    var loggerObj = {};
    
    db.getSupplySource(req, 'vast-vast', function(tag) {
        if(tag) {
            var loggerObj = logger.createLogger();
            loggerObj = logger.setPublisher(loggerObj, tag);
            loggerObj = logger.setSupplySource(loggerObj, tag);
            // sysLog.info('====================TAG ' +  tag.bids.supplyName + ' FOUND====================');
            vastRequest.getRequest(req, res, tag, loggerObj);
        } else {
            sysLog.error('====================TAG ' +  req.params.tagId + ' NOT FOUND===================');
            helper.sendHttpStatus(res, 204);
        }
    });
}