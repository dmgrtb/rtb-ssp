var request = require('request');
var mongoose = require('mongoose');
var parseString = require('xml2js').parseString;
var xml2js = require('xml2js');
var helper = require('../modules/sendRes');
var loggerParse = require('../modules/loggerParse');
var logger = require('../modules/logger.js');
var _ = require('lodash');
var forensiq = require('../services/forensiq');
var dsp = require('../auction/buildDspsRequestList');
var async = require('async');
var macro = require('../modules/macrosAnalyser');
var uap = require('node-uap');


exports.getRequest = function(req, res, tag, loggerObj) {

    dsp.buildDspsRequsetList('video-vast', req, function(adv) {

        if (adv.length > 0) {

            var queryString = adv[0].deals.endPoint;

            //replace macros with values.
            queryString = macro.analyse(req, queryString);

            var aucReq = {};
            aucReq.device = {};
            aucReq.device.geo = {};
            aucReq.site = {};
            aucReq.imp = {};
            aucReq.queryString = queryString;
            aucReq.id = mongoose.Types.ObjectId();
            aucReq.imp.bidfloor = tag.bids.minBid;

            //user agent.
            if (req.query.ua) {
                var userAgentString = uap.parse(req.query.ua);
                (userAgentString.userAgent !== undefined) ? aucReq.device.ua = userAgentString.userAgent : aucReq.device.ua = 'unknown';
                userAgentString.device.brand !== undefined ? aucReq.device.make = userAgentString.device.brand : null; 
                userAgentString.device.model !== undefined ? aucReq.device.model = userAgentString.device.model : null;  
                userAgentString.os.family !== undefined ? aucReq.device.os = userAgentString.os.family : aucReq.device.os = 'unknown'; 
            } else {
                aucReq.device.ua = 'unknown';
            }
            
            req.query.ip !== undefined ? aucReq.device.ip = req.query.ip : null;  
            
            req.query.devicetype !== undefined ? aucReq.device.deviceType = req.query.devicetype : aucReq.device.deviceType = '0';  

            if (req.query.geocountry !== undefined) {

                db.getCountryCode(req.query.geocountry, function(result) {
                    aucReq.device.geo.country = result;
                });
            } else {
                aucReq.device.geo.country = 'unknown'; 
            }

            req.query.dnt !== undefined ? aucReq.device.dnt = req.query.dnt : null; 
            (req.query.domain !== undefined && req.query.domain !== '') ? aucReq.site.domain = req.query.domain : aucReq.site.domain = 'unknown';   
            req.query.w !== undefined ? aucReq.w = req.query.w : null;  
            req.query.h !== undefined ? aucReq.h = req.query.h : null;  
            req.query.appname !== undefined ?  aucReq.appname = req.query.appname : null;  
            req.query.bundleid !== undefined ?  aucReq.bundleid = req.query.bundleid : null; 
            req.query.deviceid !== undefined ? aucReq.deviceid = req.query.deviceid : null; 
            req.query.pageurl !== undefined ? aucReq.pageurl = req.query.pageurl : null; 
            req.query.appstoreurl !== undefined ? aucReq.appstoreurl = req.query.appstoreurl : null; 
            req.query.cb !== undefined ? aucReq.cachebuster = req.query.cb : null; 
            req.query.iploclat !== undefined ? aucReq.iploclat = req.query.iploclat : null; 
            req.query.iploclong !== undefined ? aucReq.iploclong = req.query.iploclong : null; 
            req.query.categoryiab !== undefined ? aucReq.categoryiab = req.query.categoryiab : null; 
            req.headers.referer !== undefined ? aucReq.referer = req.headers.referer : null; 
            req.query.cookieid !== undefined ? aucReq.cookieid = req.query.cookieid : null; 


            loggerObj = logger.setAucReq(loggerObj, aucReq);
            loggerObj.auction.win2Bid = adv[0].deals.payout;
            loggerObj.auction.winBid = adv[0].deals.payout;
            loggerObj.auction.winDspId = adv[0]._id;


            var dspObj = {};
            dspObj._id = adv[0]._id;
            dspObj.demandPartner = adv[0].name;
            dspObj.demandDeal = adv[0].deals.name;
            dspObj.demandDealType = adv[0].deals.dealType;
            dspObj.endPoint = adv[0].deals.endPoint;
            dspObj.campaign = adv[0].campaigns.name;

            async.parallel({

                vastXml: function(callback) {
                    buildXmlVastResponse(queryString, aucReq.id, dspObj._id, function(result) {
                        callback(null, result);
                    });
                },

                forensiqScore: function(callback) {
                    if (adv[0].deals.forensiq == 'active') {
                            forensiq.check(req, loggerObj, adv, function(result) {
                            dspObj.forensiqScore = result;
                            callback(null, result);
                        });
                    } else {
                        //set default forensiq score 0 (non-suspect)
                        callback(null, 0);
                    }
                }
            },

            function(err, results) {
                if (results.forensiqScore < config.forensiqRiskScore && results.forensiqScore >= 0 && results.vastXml) {
                    dspObj.responseStatus = 'validation success';
                    dspObj.response = {};
                    dspObj.response.id = aucReq.id;
                    dspObj.response.xml = {};
                    dspObj.response.xml = encodeURIComponent(results.vastXml);
                    loggerObj = logger.setAucDspsRes(loggerObj, dspObj);
                    loggerObj.auction.dsp = loggerObj.auction.dsp[0];
                    helper.sendXml(req, res, 200, results.vastXml);
                    writeLog(loggerObj);
                } else {
                    helper.sendHttpStatus(res, 204);
                    dspObj.responseStatus = 'error status code';
                    loggerObj = logger.setAucDspsRes(loggerObj, dspObj);
                    loggerObj.auction.dsp = loggerObj.auction.dsp[0];
                    writeLog(loggerObj);
                }
            });

        } else {
            helper.sendHttpStatus(res, 204);
        }
   });
}


function buildXmlVastResponse(vastAdTagUri, auctionid, dspId, callback) {
    callback('<VAST version="2.0"><Ad id="videoAd_wrapper"><Wrapper><AdSystem>DMG</AdSystem><VASTAdTagURI><![CDATA[' + vastAdTagUri + ']]></VASTAdTagURI><Impression><![CDATA[' + config.eventTrackerUrl + '?auctionid=' + auctionid + '_' + dspId + ']]></Impression></Wrapper></Ad></VAST>');
}


function writeLog(log) {
    if (log) {
        sspLog.write(JSON.stringify(log) + "\n");
        return;
    }
}

